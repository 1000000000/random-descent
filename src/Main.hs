{-# LANGUAGE NoMonomorphismRestriction #-}
{-# LANGUAGE FlexibleContexts          #-}
{-# LANGUAGE TypeFamilies              #-}
module Main where

import Diagrams.Prelude
import Diagrams.Backend.SVG.CmdLine

import System.Random

-- | Utility function that returns 'id' when the passed boolean is true,
--   and @'const' 'mempty'@ when it's false.
whenM :: Monoid a => Bool -> a -> a
whenM True a  = a
whenM False _ = mempty

-- | Scale factor for the next level of 3-branches trees relative to the previous level
threeBranchesFactor :: Double
threeBranchesFactor = 1 / 2

-- | Scale factor for the next level of 2-branches trees relative to the previous level
twoBranchesFactor :: Double
twoBranchesFactor = 1 / sqrt 2

-- | Generates a right angled 2-branches tree where at each level a branch and its
--   children have a 1 in 3 chance of getting culled.
--   There is a 50% chance that two branches with the same parent will be congruent to each other
twoBranchesRandM :: IO (Diagram B)
twoBranchesRandM = do
  isBranch1   <- (<=(1 :: Int)) <$> randomRIO (0,2)
  isBranch2   <- (<=(1 :: Int)) <$> randomRIO (0,2)
  isSymmetric <- (<=(1 :: Int)) <$> randomRIO (1,2)
  let
    smallerBit = mappend birdFoot . translateY 1 . scale twoBranchesFactor <$> twoBranchesRandM
  branch1 <- whenM isBranch1 $ rotateBy (1/4) <$> smallerBit
  branch2 <- whenM isBranch2 $
    if isSymmetric
      then return $ branch1 # rotateBy (-1/2)
      else rotateBy (-1/4) <$> smallerBit
  return $ branch1 <> branch2
  where
    birdFoot   = vrule 1 # translateY 0.5

-- | Generates a right angled 2-branches tree where at each level a branch and its
--   children have a 1 in 3 chance of getting culled.
--   There is no guarantee that branches will be congruent to other branches at the same level.
twoBranchesRand :: IO (Diagram B)
twoBranchesRand = do
  isBranch1 <- (<=(1 :: Int)) <$> randomRIO (0,2)
  isBranch2 <- (<=(1 :: Int)) <$> randomRIO (0,2)
  let
    smallerBit = mappend birdFoot . translateY 1 . scale twoBranchesFactor <$> twoBranchesRand
  branch1 <- whenM isBranch1 $ rotateBy (1/4) <$> smallerBit
  branch2 <- whenM isBranch2 $ rotateBy (-1/4) <$> smallerBit
  return $ branch1 <> branch2
  where
    birdFoot   = vrule 1 # translateY 0.5


-- | Generates a right angled 2-branches tree where at each level a branch and its
--   children have a 1 in 3 chance of getting culled.
--   At any particular level the shape of a branch and its children is congruent to
--   those of other branches at the same level
twoBranchesRandS :: IO (Diagram B)
twoBranchesRandS = do
  isBranch1 <- (<=(1 :: Int)) <$> randomRIO (0,2)
  isBranch2 <- (<=(1 :: Int)) <$> randomRIO (0,2)
  whenM (isBranch1 || isBranch2) $ do
    smallerBit <- translateY 1 . scale twoBranchesFactor <$> twoBranchesRandS
    let
      branch = birdFoot <> smallerBit
      branch1 =
        if isBranch1
          then branch # rotateBy (1/4)
          else mempty
      branch2 =
        if isBranch2
          then branch # rotateBy (-1/4)
          else mempty
    return $ branch1 <> branch2
  where
    birdFoot   = vrule 1 # translateY 0.5

-- | Generates a right angled 2-branches tree to the given depth
twoBranches :: Integer -- ^ The depth to which the tree should be generated
           -> Diagram B
twoBranches 0 = mempty
twoBranches n = birdFoot <> smallerBit # rotateBy (1/4) <> smallerBit # rotateBy (-1/4)
  where
    smallerBit = twoBranches (n-1) # scale twoBranchesFactor # translateY 1
    birdFoot   = (hrule 1 ||| hrule 1) # centerX

-- | Generates a right angled 3-branches tree to the given depth
threeBranches :: Integer -- ^ The depth to which the tree should be generated
         -> Diagram B
threeBranches 0 = mempty
threeBranches n = birdFoot <> smallerBit <> smallerBit # rotateBy (1/4) <> smallerBit # rotateBy (-1/4)
  where
    smallerBit = threeBranches (n-1) # scale threeBranchesFactor # translateY 1
    birdFoot   = (vrule 1 === hrule 2) # translateY 0.5

main :: IO ()
main = mainWith twoBranchesRandM
